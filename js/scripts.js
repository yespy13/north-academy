/* Sección planes */

function ocultar_planes_academy()
{
  console.log("ocultar planes_academy");
  var academy = document.getElementById("id_academy");
  var pro = document.getElementById("id_pro");

  var pro_mas = document.getElementById("pro_mas");
  var pro_menos = document.getElementById("pro_menos");
  var academy_mas = document.getElementById("academy_mas");
  var academy_menos = document.getElementById("academy_menos");

  if (academy.style.display === "grid") 
  {
    academy.style.display = "none";

    pro_mas.style.visibility = "visible";
    pro_menos.style.visibility = "hidden";
    academy_mas.style.visibility = "visible";
    academy_menos.style.visibility = "hidden"; 
  } else 
  {
    academy.style.display = "grid";
    pro.style.display = "none";

    pro_mas.style.visibility = "visible";
    pro_menos.style.visibility = "hidden";
    academy_mas.style.visibility = "hidden";
    academy_menos.style.visibility = "visible"; 
  }
}
function ocultar_planes_pro()
{
  console.log("ocultar pro");
  var academy = document.getElementById("id_academy");
  var pro = document.getElementById("id_pro");

  var pro_mas = document.getElementById("pro_mas");
  var pro_menos = document.getElementById("pro_menos");
  var academy_mas = document.getElementById("academy_mas");
  var academy_menos = document.getElementById("academy_menos");

  if (pro.style.display === "grid") 
  {
    pro.style.display = "none";

    pro_mas.style.visibility = "visible";
    pro_menos.style.visibility = "hidden";
    academy_mas.style.visibility = "visible";
    academy_menos.style.visibility = "hidden"; 
  } else 
  {
    academy.style.display = "none";
    pro.style.display = "grid";

    pro_mas.style.visibility = "hidden";
    pro_menos.style.visibility = "visible";
    academy_mas.style.visibility = "visible";
    academy_menos.style.visibility = "hidden"; 
  }
}

/* Slideshow */
var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");

  if (n > slides.length) {
    slideIndex = 1
  }

  if (n < 1) {
    slideIndex = slides.length
  }

  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }

  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
} 